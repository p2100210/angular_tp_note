// update-user.component.ts

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../user.service';
import { User } from '../user.model';

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.scss'],
})
export class UpdateUserComponent implements OnInit {
  user: User | undefined;
  updatedUser: User = { id: 0, name: '', email: '', occupation: '', bio: '' };

  constructor(private route: ActivatedRoute, private router: Router, private userService: UserService) {}

  ngOnInit(): void {
    const userId = +this.route.snapshot.params['id'];
    this.userService.getUserById(userId).subscribe((user) => {
      this.user = user;
      if(user)
      this.updatedUser = { ...user }; 
    });
  }

  updateUserInfo(): void {
    this.userService.updateUser(this.updatedUser).subscribe(() => {
      this.router.navigate(['/users']);
    });
  }
}

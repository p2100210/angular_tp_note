import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { User } from '../user.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
})
export class UserListComponent implements OnInit {
  users: User[] = [];
  displayedColumns: string[] = ['id', 'name', 'email', 'occupation', 'bio', 'actions'];
  emailFilter: string = '';
  
  filteredUsers: User[] = [];

  constructor(private userService: UserService, private router: Router) {}

  ngOnInit(): void {
    this.userService.getUsers().subscribe((users) => {
      this.users = users;
      this.applyFilter();
    });
  }

  getUsers(): void {
    this.userService.getUsers().subscribe((users) => {
      this.users = users; this.applyFilter();});
  }

  viewDetails(userId: number): void {
    // Naviguer vers la page des détails de l'utilisateur
    this.router.navigate(['/user', userId]);
  }

  updateUser(userId: number): void {
    // Naviguer vers la page de mise à jour de l'utilisateur
    this.router.navigate(['/update', userId]);
  }

  deleteUser(userId: number): void {
    // Demander une confirmation avant de supprimer
    const isConfirmed = window.confirm('Voulez-vous vraiment supprimer cet utilisateur?');
    
    if (isConfirmed) {
      // Appeler le service pour supprimer l'utilisateur
      this.userService.deleteUser(userId).subscribe(() => {
        // Rafraîchir la liste des utilisateurs après la suppression
        this.getUsers();
      });
    }
  }

  applyFilter(): void {
    this.filteredUsers = this.users.filter(user =>
      user.email.toLowerCase().includes(this.emailFilter.toLowerCase())
    );
  }
}
// add-user.component.ts

import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';
import { User } from '../user.model';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss'],
})
export class AddUserComponent {
  newUser: User = { id: 0, name: '', email: '', occupation: '', bio: '' };

  constructor(private router: Router, private userService: UserService) {}

  addUser(): void {
    this.userService.addUser(this.newUser).subscribe(() => {
      this.router.navigate(['/users']);
    });
  }
}

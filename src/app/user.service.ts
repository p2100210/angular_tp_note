import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { User } from './user.model';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private apiUrl = 'https://65884eaa90fa4d3dabf9ba0e.mockapi.io';
  private users: User[] = []; 

  constructor(private http: HttpClient) {}

  getUsers(): Observable<User[]> {
    if (this.users.length === 0) {
      return this.http.get<User[]>(`${this.apiUrl}/users`).pipe(
        catchError((error) => {
          console.error('Error fetching users:', error);
          return of([]);
        })
      );
    } else {
      return of(this.users);
    }
  }

  getUserById(id: number): Observable<User | undefined> {
    if (this.users.length === 0) {
      return this.http.get<User>(`${this.apiUrl}/users/${id}`).pipe(
        catchError((error) => {
          console.error(`Error fetching user with id ${id}:`, error);
          return of(undefined);
        })
      );
    } else {
      const user = this.users.find((u) => u.id === id);
      return of(user);
    }
  }

  addUser(newUser: User): Observable<User> {
    return this.http.post<User>(`${this.apiUrl}/users`, newUser).pipe(
      catchError((error) => {
        console.error('Error adding user:', error);
        return of(newUser); 
      }),
      map((addedUser) => {
        this.users.push(addedUser);
        return addedUser;
      })
    );
  }

  updateUser(updatedUser: User): Observable<User> {
    return this.http.put<User>(`${this.apiUrl}/users/${updatedUser.id}`, updatedUser).pipe(
      catchError((error) => {
        console.error(`Error updating user with id ${updatedUser.id}:`, error);
        return of(updatedUser);
      }),
      map((newUserData) => {
        const index = this.users.findIndex((user) => user.id === newUserData.id);
        if (index !== -1) {
          this.users[index] = newUserData;
        }
        return newUserData;
      })
    );
  }

  deleteUser(userId: number): Observable<void> {
    const url = `${this.apiUrl}/users/${userId}`;

    return this.http.delete<void>(url);
  }
}

import { User } from './user.model';

export const userData: User[] = [
  { id: 1, name: 'John Doe', occupation: 'Developer', email: 'john.doe@example.com', bio: 'Some bio text' },
];
